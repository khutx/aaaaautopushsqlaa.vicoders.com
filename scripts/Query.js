import * as _ from "lodash";
import fs from "fs";
import models from "../models";
require('dotenv').config()
const spawn = require('child-process-promise').spawn;
import randomstring from "randomstring";

export class Query {

    moveDir(website, link = '') {
        let path = `/var/www/web/${website}/workspace${link}`
        process.chdir(path);
    }

    convertCommand(cmd) {
        cmd = cmd.replace(/"/gi, '');
        cmd = _.split(cmd, ' ');
        return { cmd: cmd[0], options: _.drop(cmd) };
    }

    convertObjectToString(obj) {
        return new Promise((resolve, reject) => {
            let string = new String();
            _.mapKeys(obj, (value, key) => {
                string = `${string}${key}=${value}\n`;
            });

            resolve(string);
        });
    }

    findFile(file) {
        return new Promise(async(resolve, reject) => {
            try {
                let cmd = this.convertCommand(`find -name ${file} -type f`);
                let sp = await spawn(cmd['cmd'], cmd['options'], { capture: ['stdout', 'stderr'] });
                if (_.isEmpty(sp.stdout) && _.isEmpty(sp.stderr)) {
                    throw new Error(`${file} not found`);
                }
                let list = _.split(sp.stdout, "\n");
                list = _.remove(list, (n) => {
                    return !_.isEmpty(n);
                });
                resolve(list);
            } catch (e) {
                reject(e);
            }
        });
    }

    readFile(path) {
        return new Promise(async(resolve, reject) => {
            fs.readFile(path, (err, data) => {
                if (err) {
                    reject(err);
                }
                resolve(data);
            });
        });
    }

    readEnv(path) {
        return new Promise(async(resolve, reject) => {
            let obj = {};
            fs.readFile(path, (err, data) => {
                if (err) {
                    reject(err);
                }
                data = _.split(data, '\n');
                data = _.remove(data, (n) => {
                    return !_.isEmpty(n) && n.indexOf('#') && n.indexOf('\r');
                });
                for (let i in data) {
                    data[i] = _.split(data[i], '=');
                    obj[data[i][0]] = data[i][1];
                }
                resolve(obj);
            });
        });
    }

    readConfig(path) {
        return new Promise(async(resolve, reject) => {
            let obj = {};
            fs.readFile(path, (err, data) => {
                if (err) {
                    reject(err);
                }
                data = _.split(data, '\n');
                data = _.remove(data, (n) => {
                    return !n.indexOf('define');
                });
                for (let i in data) {
                    data[i] = data[i].replace(/ /gi, '');
                    data[i] = data[i].replace(/'/gi, '');
                    data[i] = data[i].slice(7, -2);
                    data[i] = _.split(data[i], ',');
                    obj[data[i][0]] = data[i][1];
                }
                resolve(obj);
            });
        });
    }

    findFolder(folder) {
        return new Promise(async(resolve, reject) => {
            try {
                let cmd = this.convertCommand(`find -name ${folder} -type d`);
                let sp = await spawn(cmd['cmd'], cmd['options'], { capture: ['stdout', 'stderr'] });
                if (_.isEmpty(sp.stdout) && _.isEmpty(sp.stderr)) {
                    throw new Error(`${folder} not found`);
                }
                let list = _.split(sp.stdout, "\n");
                list = _.remove(list, (n) => {
                    return !_.isEmpty(n);
                });
                resolve(list);
            } catch (e) {
                reject(e);
            }
        });
    }

    filterCommand(cmd) {
        return new Promise((resolve, reject) => {
            let result = this.convertCommand(cmd);
            switch (result['cmd']) {
                case 'php':
                case 'composer':
                case 'npm':
                    resolve(result);
                    break;
                default:
                    reject(new Error(`${result['cmd']} command not allowed`));
                    break;
            }
        })
    }

    getInfomation(website) {
        return new Promise(async(resolve, reject) => {
            try {
                this.moveDir(website);
                let cmd1 = this.convertCommand('find -name "wp-content" -type d');
                let wp = await spawn(cmd1['cmd'], cmd1['options'], { capture: ['stdout', 'stderr'] });
                if (!_.isEmpty(wp.stdout)) {
                    resolve({ website: website, framework: 'wordpress' });
                }

                let cmd2 = this.convertCommand('find -name "app" -type d');
                let lv = await spawn(cmd2['cmd'], cmd2['options'], { capture: ['stdout', 'stderr'] });
                if (!_.isEmpty(lv.stdout)) {
                    resolve({ website: website, framework: 'laravel' });
                }

                resolve({ website: website, framework: "framework not defined" });
            } catch (e) {
                reject(e);
            }
        })
    }

    runCommand(website, command) {
        return new Promise(async(resolve, reject) => {
            try {
                this.moveDir(website);
                let cmd = await this.filterCommand(command);
                let sp = await spawn(cmd['cmd'], cmd['options'], { capture: ['stdout', 'stderr'] });
                resolve({ stdout: sp.stdout, stderr: sp.stderr });
            } catch (e) {
                reject(e);
            }
        });
    }

    createUserDb(website) {
        return new Promise(async(resolve, reject) => {
            try {
                let dbname = website.replace(/[\.|\-]/gi, '');
                let password = randomstring.generate(8);
                let data = {
                    Host: 'localhost',
                    User: dbname,
                    plugin: 'mysql_native_password',
                    authentication_string: password,
                    password_last_changed: new Date(),

                }

                let user = await models.user.create(data);
                resolve(data);
            } catch (e) {
                reject(e);
            }
        });

    }
}